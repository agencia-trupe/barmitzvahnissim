<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutorizacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autorizacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('acomodacao');
            $table->string('transporte');
            $table->string('nome_condominio');
            $table->string('numero_casa');
            $table->string('nome_responsavel');
            $table->string('celular_responsavel');
            $table->string('nome_filho');
            $table->string('rg_filho');
            $table->string('nome_mae');
            $table->string('rg_mae');
            $table->string('nome_pai');
            $table->string('rg_pai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('autorizacoes');
    }
}
