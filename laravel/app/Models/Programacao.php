<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Programacao extends Model
{
    protected $table = 'programacao';

    protected $guarded = ['id'];

}
