<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Orientacoes extends Model
{
    protected $table = 'orientacoes';

    protected $guarded = ['id'];

}
