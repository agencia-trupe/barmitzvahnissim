<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\OrientacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Orientacoes;

class OrientacoesController extends Controller
{
    public function index()
    {
        $registro = Orientacoes::first();

        return view('painel.orientacoes.edit', compact('registro'));
    }

    public function update(OrientacoesRequest $request, Orientacoes $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.orientacoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
