<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Autorizacao;

class AutorizacoesController extends Controller
{
    public function index()
    {
        $autorizacoes = Autorizacao::orderBy('id', 'DESC')->get();
        return view('painel.autorizacoes', compact('autorizacoes'));
    }

    public function destroy(Autorizacao $autorizacao)
    {
        try {

            $autorizacao->delete();
            return redirect()->route('painel.autorizacoes.index')
                             ->with('success', 'Autorização excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir autorização: '.$e->getMessage()]);

        }
    }
}
