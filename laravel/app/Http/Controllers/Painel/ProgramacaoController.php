<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProgramacaoRequest;
use App\Http\Controllers\Controller;

use App\Models\Programacao;

class ProgramacaoController extends Controller
{
    public function index()
    {
        $registro = Programacao::first();

        return view('painel.programacao.edit', compact('registro'));
    }

    public function update(ProgramacaoRequest $request, Programacao $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.programacao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
