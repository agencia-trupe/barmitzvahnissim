<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home');
    }

    public function autorizacao()
    {
        return view('frontend.autorizacao.index');
    }

    public function autorizacaoForm(Request $request)
    {
        $dados = $request->all();
        return view('frontend.autorizacao.form', compact('dados'));
    }

    public function autorizacaoEnvio(Request $request)
    {
        $rules = [
            'nome_filho' => 'required',
            'rg_filho'   => 'required',
        ];

        if ($request->acomodacao == 'sim') {
            $rules = array_merge($rules, [
                'nome_condominio'     => 'required',
                'numero_casa'         => 'required',
                'nome_responsavel'    => 'required',
                'celular_responsavel' => 'required',
            ]);
        } else {
            $rules = array_merge($rules, [
                'nome_responsavel'    => 'required',
                'celular_responsavel' => 'required',
            ]);
        }
        if ($request->transporte != 'nao') {
            $rules = array_merge($rules, [
                'nome_mae' => 'required',
                'rg_mae'   => 'required',
                'nome_pai' => 'required',
                'rg_pai'   => 'required',
            ]);
        }

        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

        $dados = $request->all();
        try {
            $autorizacao = \App\Models\Autorizacao::create($dados);
            return view('frontend.autorizacao.imprimir', compact('autorizacao'));
        } catch (\Exception $e) {
            dd('Ocorreu um erro. Tente novamente');
        }
    }

    public function ficha()
    {
        return view('frontend.ficha');
    }

    public function programacao()
    {
        return view('frontend.programacao')->with([
            'programacao' => \App\Models\Programacao::first()
        ]);
    }

    public function levar()
    {
        return view('frontend.levar')->with([
            'orientacoes' => \App\Models\Orientacoes::first()
        ]);
    }

    public function fotos()
    {
        $fotos = \App\Models\Foto::ordenados()->get();
        return view('frontend.fotos', compact('fotos'));
    }
}
