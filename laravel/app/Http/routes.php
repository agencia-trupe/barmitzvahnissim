<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('autorizacao', 'HomeController@autorizacao')->name('autorizacao');
    Route::get('autorizacao/formulario', 'HomeController@autorizacaoForm')->name('autorizacao.formulario');
    Route::post('autorizacao', 'HomeController@autorizacaoEnvio')->name('autorizacao.envio');
    Route::get('ficha-de-saude', 'HomeController@ficha')->name('ficha-de-saude');
    Route::get('programacao', 'HomeController@programacao')->name('programacao');
    Route::get('o-que-levar', 'HomeController@levar')->name('o-que-levar');
    Route::get('fotos', 'HomeController@fotos')->name('fotos');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::resource('autorizacoes', 'AutorizacoesController');
        Route::resource('programacao', 'ProgramacaoController');
        Route::resource('orientacoes', 'OrientacoesController');
        Route::resource('fotos', 'FotosController');
        Route::resource('usuarios', 'UsuariosController');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
