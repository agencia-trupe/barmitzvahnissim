(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.fotos = function() {
        $('.fancybox').fancybox({
            padding: 10,
            helpers: {
                overlay: { locked: false }
            }
        });
    };

    App.init = function() {
        this.fotos();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
