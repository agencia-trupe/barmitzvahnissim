@extends('frontend.common.template')

@section('content')

    <div class="main ficha">
        <div class="center">
            <a href="{{ url('assets/ficha-de-saude.pdf') }}" class="btn">DOWNLOAD DA FICHA DE SAÚDE</a>
            <p>Esta ficha deverá ser preenchida em data próxima ao embarque para que as informações nela contidas estejam atualizadas.</p>
            <p>
                Preencher com letra legível, scanear e enviar para:
                <a href="mailto:barmitzvanino@gmail.com">barmitzvanino@gmail.com</a>
                juntamente com uma cópia simples do RG do aluno.
            </p>
            <p>
                Só embarcarão os alunos que tiverem apresentado:<br>
                <span>1.</span> Autorização de viagem preenchida e assinada<br>
                <span>2.</span> Ficha de saúde preenchida e enviada por e-mail (ou impressa e assinada)<br>
                <span>3.</span> Cópia do RG do aluno scaneada e enviada por e-mail (ou impressa e entregue junto com a autorização de viagem)
            </p>
        </div>
    </div>

@endsection
