@extends('frontend.common.template')

@section('content')

    <div class="main autorizacao">
        <div class="center">
            @if($autorizacao->transporte == 'nao')
            <h2>Obrigado pelas informações. Nos vemos lá!</h2>
            @else
            <h2>AUTORIZAÇÃO DE VIAGEM</h2>
            <p style="margin-bottom: .5em">Autorizamos nosso(a) filho(a)</p>
            <p><strong>{{ $autorizacao->nome_filho }}, RG {{ $autorizacao->rg_filho }}</strong></p>
            <p style="margin-top: .5em">a viajar desacompanhado(a) à Praia da Baleia<br>nos dias 3, 4 e 5 de fevereiro de 2017.</p>
            <p style="margin: 1.5em auto .5em">Mãe:</p>
            <p style="margin-top: 0"><strong>{{ $autorizacao->nome_mae }}, RG {{ $autorizacao->rg_mae }}</strong></p>
            <p style="margin: 1.5em auto .5em">Pai:</p>
            <p style="margin-top: 0"><strong>{{ $autorizacao->nome_pai }}, RG {{ $autorizacao->rg_pai }}</strong></p>

            <div class="line"></div>
            <h2>ACOMODAÇÃO</h2>
            @if($autorizacao->acomodacao === 'sim')
            <p><strong>Condomínio:</strong> {{ $autorizacao->nome_condominio }}, número {{ $autorizacao->numero_casa }}</p>
            @endif
            <p><strong>Responsável:</strong> {{ $autorizacao->nome_responsavel }}, celular {{ $autorizacao->celular_responsavel }}</p>

            <a href="javascript:window.print();" class="btn">IMPRIMIR</a>
            <p class="texto-impressao">Deixar esta ficha impressa e assinada com seu(a) filho(a) no dia da viagem: 3 FEV 2017</p>
            @endif

        </div>
    </div>

@endsection
