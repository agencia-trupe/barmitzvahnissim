@extends('frontend.common.template')

@section('content')

    <div class="main autorizacao">
        <div class="center">
            <form action="{{ route('autorizacao.formulario') }}" method="GET">
                <h2>ACOMODAÇÃO</h2>
                <h3>Possui local de acomodação?</h3>
                <div class="selecao">
                    <input type="radio" name="acomodacao" id="acomodacao-sim" value="sim" checked>
                    <label for="acomodacao-sim">Sim</label>
                    <input type="radio" name="acomodacao" id="acomodacao-nao" value="nao">
                    <label for="acomodacao-nao">Não</label>
                </div>
                <p>Caso seu(a) filho(a) não tenha local de acomodação por favor nos informe que teremos o maior prazer em auxiliá-lo.</p>

                <div class="line"></div>

                <h2>TRANSPORTE</h2>
                <h3>Utilizará o transporte oferecido em ônibus de viagem?</h3>
                <div class="selecao">
                    <input type="radio" name="transporte" id="transporte-sim1" value="ida-volta" checked>
                    <label for="transporte-sim1">Sim - ida e volta</label>
                    <input type="radio" name="transporte" id="transporte-sim2" value="ida">
                    <label for="transporte-sim2">Sim - somente ida</label>
                    <input type="radio" name="transporte" id="transporte-sim3" value="volta">
                    <label for="transporte-sim3">Sim - somente volta</label>
                    <input type="radio" name="transporte" id="transporte-nao" value="nao">
                    <label for="transporte-nao">Não - nos encontraremos lá!</label>
                </div>
                <input type="submit" value="PROSSEGUIR">
            </form>
        </div>
    </div>

@endsection
