@extends('frontend.common.template')

@section('content')

    <div class="main autorizacao">
        <div class="center">
            <form action="{{ route('autorizacao.envio') }}" method="POST">
                @if(count($errors) > 0)
                <div class="erro">Preencha todos os campos corretamente.</div>
                @endif
                @if($dados['acomodacao'] === 'sim')
                <h2>DADOS DO LOCAL DA ACOMODAÇÃO</h2>
                <input type="text" name="nome_condominio" @if($errors->has('nome_condominio')) class="erro" @endif value="{{ old('nome_condominio') }}" placeholder="nome do condomínio" required>
                <input type="text" name="numero_casa" @if($errors->has('numero_casa')) class="erro" @endif value="{{ old('numero_casa') }}" placeholder="número da casa" required>
                <input type="text" name="nome_responsavel" @if($errors->has('nome_responsavel')) class="erro" @endif value="{{ old('nome_responsavel') }}" placeholder="nome do responsável no local da acomodação" required>
                <input type="text" name="celular_responsavel" @if($errors->has('celular_responsavel')) class="erro" @endif value="{{ old('celular_responsavel') }}" placeholder="celular do responsável durante a acomodação" required>
                @else
                <h2>PARA ACOMODAÇÃO</h2>
                <h3>Por favor, informe seu telefone de contato para auxiliarmos com a acomodação</h3>
                <input type="text" name="nome_responsavel" @if($errors->has('nome_responsavel')) class="erro" @endif value="{{ old('nome_responsavel') }}" placeholder="nome do responsável" required>
                <input type="text" name="celular_responsavel" @if($errors->has('celular_responsavel')) class="erro" @endif value="{{ old('celular_responsavel') }}" placeholder="celular do responsável" required>
                @endif

                @if($dados['transporte'] === 'nao')
                <input type="text" name="nome_filho" @if($errors->has('nome_filho')) class="erro" @endif value="{{ old('nome_filho') }}" placeholder="nome completo do(a) filho(a)" required>
                <input type="text" name="rg_filho" @if($errors->has('rg_filho')) class="erro" @endif value="{{ old('rg_filho') }}" placeholder="RG do(a) filho(a)" required>
                @else
                <div class="line"></div>
                <h2>AUTORIZAÇÃO DE VIAGEM</h2>
                <p style="margin-bottom: .5em">Autorizamos nosso(a) filho(a)</p>
                <input type="text" name="nome_filho" @if($errors->has('nome_filho')) class="erro" @endif value="{{ old('nome_filho') }}" placeholder="nome completo do(a) filho(a)" required>
                <input type="text" name="rg_filho" @if($errors->has('rg_filho')) class="erro" @endif value="{{ old('rg_filho') }}" placeholder="RG do(a) filho(a)" required>
                <p style="margin-top: .5em">a viajar desacompanhado(a) à Praia da Baleia<br>nos dias 3, 4 e 5 de fevereiro de 2017.</p>
                <p style="margin: 1.5em auto .5em">Mãe:</p>
                <input type="text" name="nome_mae" @if($errors->has('nome_mae')) class="erro" @endif value="{{ old('nome_mae') }}" placeholder="nome completo da mãe" required>
                <input type="text" name="rg_mae" @if($errors->has('rg_mae')) class="erro" @endif value="{{ old('rg_mae') }}" placeholder="RG da mãe" required>
                <p style="margin: 1.5em auto .5em">Pai:</p>
                <input type="text" name="nome_pai" @if($errors->has('nome_pai')) class="erro" @endif value="{{ old('nome_pai') }}" placeholder="nome completo do pai" required>
                <input type="text" name="rg_pai" @if($errors->has('rg_pai')) class="erro" @endif value="{{ old('rg_pai') }}" placeholder="RG do pai" required>
                @endif

                {!! csrf_field() !!}
                <input type="hidden" name="acomodacao" value="{{ $dados['acomodacao'] }}">
                <input type="hidden" name="transporte" value="{{ $dados['transporte'] }}">

                <input type="submit" value="PROSSEGUIR">
            </form>
        </div>
    </div>

@endsection
