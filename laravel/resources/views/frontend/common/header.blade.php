    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>
            <nav>
                <a href="{{ route('autorizacao') }}" @if(str_is('autorizacao*', Route::currentRouteName())) class="active" @endif>AUTORIZAÇÃO DE VIAGEM & ACOMODAÇÃO</a>
                <a href="{{ route('ficha-de-saude') }}" @if(Route::currentRouteName() == 'ficha-de-saude') class="active" @endif>FICHA DE SAÚDE</a>
                <span class="tablet-break"></span>
                <a href="{{ route('programacao') }}" @if(Route::currentRouteName() == 'programacao') class="active" @endif>PROGRAMAÇÃO</a>
                <a href="{{ route('o-que-levar') }}" @if(Route::currentRouteName() == 'o-que-levar') class="active" @endif>O QUE LEVAR?</a>
                <a href="{{ route('fotos') }}" @if(Route::currentRouteName() == 'fotos') class="active" @endif>GALERIA DE FOTOS</a>
            </nav>
        </div>
    </header>
