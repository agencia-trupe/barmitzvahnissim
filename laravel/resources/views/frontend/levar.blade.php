@extends('frontend.common.template')

@section('content')

    <div class="main levar">
        <div class="center">
            <a href="javascript:window.print();" class="btn">IMPRIMIR LISTA E ORIENTAÇÕES</a>
            <h2>BAR MITZVAH NISSIM SARFATY</h2>
            <h1>Orientações</h1>
            {!! $orientacoes->texto !!}
        </div>
    </div>

@endsection
