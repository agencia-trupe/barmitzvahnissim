@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <style>
        h3 { display:none; }
        @media print {
            .btn-success { display:none; }
            h3 { display:block; }
            thead { display:none; }
            tr td:nth-child(3) { display:none; }
        }
    </style>

    <legend>
        <h3>BAR MITZVAH NISSIM SARFATY</h3>
        <h2>
            Autorizações
            <a href="javascript:window.print()" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-print" style="margin-right:10px;"></span>Imprimir</a>
        </h2>
    </legend>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Autorização</th>
                <th></th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $opcoes = [
                    'nao'       => 'Não',
                    'ida'       => 'Sim - somente ida',
                    'volta'     => 'Sim - somente volta',
                    'ida-volta' => 'Sim - ida e volta'
                ];
            ?>
            @foreach($autorizacoes as $autorizacao)
            <?php $transporte = $opcoes[$autorizacao->transporte]; ?>
            <tr>
                <td style="vertical-align: top !important;">
                    <p style="margin:0">
                        <strong>Filho:</strong> {{ $autorizacao->nome_filho }}, RG {{ $autorizacao->rg_filho }}<br>
                        <strong>Transporte:</strong> {{ $transporte }}<br>
                        @if($autorizacao->acomodacao == 'sim')
                            <strong>Condomínio:</strong> {{ $autorizacao->nome_condominio }}, número {{ $autorizacao->numero_casa }}<br>
                        @else
                            Não possui acomodação.
                        @endif
                </td>
                <td style="vertical-align: top !important;">
                    <p style="margin:0">
                        <strong>Responsável:</strong> {{ $autorizacao->nome_responsavel }}, celular {{ $autorizacao->celular_responsavel }}<br>
                        @if($autorizacao->transporte != 'nao')
                        <strong>Mãe:</strong> {{ $autorizacao->nome_mae }}, RG {{ $autorizacao->rg_mae }}<br>
                        <strong>Pai:</strong> {{ $autorizacao->nome_pai }}, RG {{ $autorizacao->rg_pai }}
                        @endif
                    </p>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.autorizacoes.destroy', $autorizacao->id],
                        'method' => 'delete'
                    ]) !!}
                    <div class="btn-group btn-group-sm">
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection
