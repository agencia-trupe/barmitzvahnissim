@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Orientações</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.orientacoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.orientacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
