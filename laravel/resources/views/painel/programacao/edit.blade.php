@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Programação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.programacao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.programacao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
